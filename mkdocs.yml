site_name: "eSkool, Le Libre pour l'Éducation"
site_url: https://eskool.gitlab.io/accueil/
site_author: eSkool, Rodrigo Schwencke
site_description: >-
  This is my nice Site!

# Repository
repo_name: eskool/accueil
repo_url: https://gitlab.com/eskool/accueil
# edit_uri: ""
edit_uri: tree/main/docs/

# Copyright
copyright: Copyleft <span style="font-weight:700;font-size:1.2em;">&#127279;</span> 2023 - eSkool - Rodrigo Schwencke - Licence CC BY-NC-SA 4.0

# Configuration
theme:
  name: material
  custom_dir: overrides
  logo: assets/images/eskool-nsi-blanc.svg
  favicon: assets/images/eskool-nsi-blanc.svg
  language: fr
  features:
    # empêche nouvelle requête:
    # ! ne pas activer 'navigate.instant':
    # - navigation.instant
    - navigation.tabs
    # - navigation.expand
    - navigation.top
    # Menus intégrés, ou pas:
    # - toc.integrate
    - header.autohide
    - navigation.tabs.sticky
    - navigation.sections
    - content.code.annotate
    # laisser 'navigation.indexes commenté, sinon les fichiers par défaut /index.md 
    # ne sont pas automatiquement affichés dans menu gauche
    # - navigation.indexes
  palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: blue
        accent: blue
        toggle:
            icon: material/weather-night
            name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: red
        accent: red
        toggle:
            icon: material/weather-sunny
            name: Passer au mode jour

  favicon: assets/images/favicon.png
  icon:
    logo: logo
    admonition:
      note: octicons/tag-16
      abstract: octicons/checklist-16
      info: octicons/info-16
      tip: octicons/squirrel-16
      success: octicons/check-16
      question: octicons/question-16
      warning: octicons/alert-16
      failure: octicons/x-circle-16
      danger: octicons/zap-16
      bug: octicons/bug-16
      example: octicons/beaker-16
      quote: octicons/quote-16

# Extensions
markdown_extensions:
  - admonition
  - abbr
  - attr_list
  - def_list
  - footnotes
  - md_in_html
  - mkdocs_graphviz
      # light_theme: 995522
      # dark_theme: chartreuse
      # color: 02f514
      # bgcolor: 0000FF or none
      # graph_color: FF0000
      # graph_fontcolor: 00FF00
      # node_color: FF0000
      # node_fontcolor: 0000FF
      # edge_color: FF0000
      # edge_fontcolor: 00FF00
  - mkdocs_asy
  - meta
  - pymdownx.arithmatex:
      generic: true
      smart_dollar: false
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
      options:
        custom_icons:
          - overrides/.icons
  - pymdownx.highlight:
      linenums: None
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.mark
  # Conflit avec snippets
  # - pymdownx.smartsymbols
  - pymdownx.snippets
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_div_format
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - toc:
      permalink: ⚓︎
      toc_depth: 3

# Plugins
plugins:
  - search
  # - kroki
  - macros:
      site_url: https://eskool.gitlab.io/1nsi/
  # - markmap
  # - mkdocs-jupyter:
  #     include_source: True
  # - tooltips
  
  #- page-to-pdf # should be last
      # execute: True

  # PAS le contraire : index.md -> bases.md
  # car n'affiche pas les menus
  # - redirects:
  #     redirect_maps:
  #       web/ihm_python.md: python/ihm.md

# Customization
extra:
  massilia:
    badges:
      default: deeppink
    colors:
      demo1: ff0000 0000ff b0b000 fdfd75 0000ff ff0000
    styles:
      test:
        light:
          font-family: Source Code Pro 
          background-color: purple
          color: red
          border-radius: 20px
          box-shadow : 10px 10px 5px orange
        dark:
          font-family: Source Code Pro
          background-color: cyan
          color: darkblue
          border-radius: 20px
          box-shadow : 10px 10px 5px orange
  social:
    - icon: fa/creative-commons
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/creative-commons-by
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/creative-commons-nc-eu
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/creative-commons-sa
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      name: License CC BY-NC-SA 4.0
    - icon: fa/paper-plane
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      link: mailto:contact.eskool@gmail.com
      name: Envoyer un email
    # - icon: fontawesome/brands/linux
    - icon: sk/matrix
      link: https://riot.im/app/#/room/#eskool:matrix.org
      name: "Rejoignez nous sur la Matrice:\n#eskool:matrix.org"
    - icon: fa/linux
      link: https://linuxfr.org/
      name: Linux is Cool!
    - icon: fa/gitlab
      link: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
      link: https://gitlab.com/eskool/1nsi
      name: dépôt gitlab
    - icon: fa/university
      link: https://eskool.gitlab.io/accueil
      name: The eSkool Initiative
  raw_url: https://eskool.github.io/1nsi/-/raw/main/docs/
  io_url: https://eskool.gitlab.io/1nsi
  site_url: https://eskool.gitlab.io/1nsi/

extra_css:
    - https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css
    - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/pyscript.css
    - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/massilia.css

extra_javascript:
  # for MathJax (mathjax.js config file MUST precede 'polyfill' and 'cdn')
  # - assets/javascripts/mathjax.js
  - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/mathjax.js
  - https://polyfill.io/v3/polyfill.js?features=es5,es6,es7,es8&flags=gated
  - https://cdn.jsdelivr.net/npm/mathjax@3.2.2/es5/tex-mml-chtml.js

  - https://cdnjs.cloudflare.com/ajax/libs/mermaid/9.1.7/mermaid.min.js
  # - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/javascripts/massilia.js
  - https://cdn.plot.ly/plotly-2.6.3.min.js

nav:
  - Accueil:
    - Accueil: index.md
    - Lycée:
      - Terminale:
        - Spé NSI <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/tnsi/
        - Spé Mathématiques <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/tmaths/
        - Option Mathématiques Complémentaires NSI <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/tmathsco/
        - Option Mathématiques Expertes NSI <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/tmathsexpertes/
        - Histoire <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/thistoire/
        - Géographie <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/tgeographie/
      - 1ère:
        - Spé NSI <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/1nsi/
        - Spé Mathématiques <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/1maths/
        - Histoire <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/1histoire/
        - Géographie <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/1geographie/
      - 2nde:
        - Mathématiques <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/2maths/
        - Histoire <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/2histoire/
        - Géographie <i class="fa fa-external-link-alt" aria-hidden="true"></i>: https://eskool.gitlab.io/2geographie/
  - Bac à Sable: sandbox.md
  
