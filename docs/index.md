# eSkool Accueil

Bienvenue sur le Projet eSkool: Le Libre pour l'Éducation!  

## Vous aimez ce projet? Vous croyez en une Éducation Libre ?

<span style="margin-left:2em;"></span>:fa-arrow-right: Soutenez-nous :

* en ajoutant des étoiles :star: :star: :star: au [dépôt GitLab](https://gitlab.com/eskool/accueil/) de ce projet : çà motive! :smile:
* Faites nous connaître auprès de vos proches

## Vous souhaitez nous aider? 

En acceptant de publier des contenus pour votre discipline, sous [Licence CC-BY-NC-SA (CC - Creative Commons, BY- Citation de l'auteur, NC - Non Commercial, SA - Share Alike - Partage dans les Mêmes Conditions)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)?

<center><a href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr"><img src="CC-BY-NC-SA.svg" style="width:100px;height:auto;"/></a></center>

<span style="margin-left:2em;"></span>:fa-arrow-right: Contactez-nous (:fa-paper-plane:  email : contact.eskool@gmail.com)! 

Nous serons heureux de travailler avec vous, et tâcherons de développer des fonctionnalités adaptées à votre discipline !